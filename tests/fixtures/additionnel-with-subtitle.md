## idChoixChapitres

---
title: >-
  Choix des différents chapitres
credits: >-
  Séminaire IMPEC
keywords: chapitre,choix,thématique,angle
type: texte
lang: fr
link:
link-archive:
embed:
zotero:
date: 2020-12-01
source: auteur
priority: lowpriority

session:
protagonist:
artefact:
interviewee:
interviewer:
transcriber:
transcription-link:
duration:
timecode:

---

### Choix des différents chapitres

En 2017, la mise en commun des idées concernant les différents angles d'étude à privilégier a permis, dans un premier temps, de dégager trois thématiques de départ&nbsp;: l'attention, la corporéité et la politesse. La proposition a été faite aux participant·e·s de se regrouper autour de l'un de ces trois angles. En septembre 2018, la thématique de quatre nouveaux chapitres a été décidée&nbsp;: la comparaison des effets de présence par artefact, la co-construction d'une intelligence collective, la formation à la recherche et l'effet dynamique de groupe. De nouveaux groupes se sont constitués pour le travail en commun.

Le choix des auteur·rice·s s’est effectué librement et le travail d’analyse s’est donc réalisé en sous-groupes. L’idée étant que chaque sous-groupe puisse convoquer un angle d’étude original et que chaque chapitre soit susceptible de mettre en lumière un aspect vu sous un certain angle, qui ne serait pas celui de l’ouvrage dans son ensemble.
