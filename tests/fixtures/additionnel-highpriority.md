## idEthologieReflexiveDev

---
title: >-
  Éthologie réflexive
credits: >-
  Séminaire IMPEC
keywords: éthologie réflexive,Jacques Cosnier
type: texte-definition
lang: fr
link:
link-archive:
embed:
zotero:
date: 2020-12-01
source: auteur
priority: highpriority

session:
protagonist:
artefact:
interviewee:
interviewer:
transcriber:
transcription-link:
duration:
timecode:

---

L'auteur explique que&nbsp;:

> la méthode éthologique est particulièrement heuristique dans les approches où l'observation est essentielle par exemple dans la clinique, la psychologie du développement, la psychologie sociale, c'est-à-dire partout où les communications interindividuelles constituent un objet d'étude privilégié [dans @hotier_entretien_2001].
