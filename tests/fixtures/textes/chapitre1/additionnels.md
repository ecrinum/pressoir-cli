## idEthologieReflexiveDev

---
title: >-
  Éthologie réflexive
credits: >-
  Séminaire IMPEC
keywords: éthologie réflexive,Jacques Cosnier
type: texte
lang: fr
link:
link-archive:
embed:
zotero:
date: 2020-12-01
source: auteur
priority: highpriority

session:
protagonist:
artefact:
interviewee:
interviewer:
transcriber:
transcription-link:
duration:
timecode:

---
L'auteur explique que&nbsp;:

> la méthode éthologique est particulièrement heuristique dans les approches où l'observation est essentielle par exemple dans la clinique, la psychologie du développement, la psychologie sociale, c'est-à-dire partout où les communications interindividuelles constituent un objet d'étude privilégié [dans @hotier_entretien_2001].


## idEthologieReflexiveApp

---
title:  >-
  Éthologie compréhensive
credits: >-
  Séminaire IMPEC
keywords: éthologie reflexive,Jacques Cosnier,ethnographie de la communication,Dell H. Hymes,John J. Gumperz,microsociologie,Erving Goffman,ethnométhodologie,Harold Garfinkel
type: texte
lang: fr
link:
link-archive:
embed:
zotero:
date: 2020-12-01
source: auteur
priority: lowpriority

session:
protagonist:
artefact:
interviewee:
interviewer:
transcriber:
transcription-link:
duration:
timecode:

---
[Cosnier]{.personnalite idsp="Cosnier, Jacques" idbnf="https://catalogue.bnf.fr/ark:/12148/cb11897776d"} note aussi que «&nbsp;cette éthologie humaine rejoint évidemment l'ethnographie de la communication de [Hymes]{.personnalite idsp="Hymes, Dell H." idbnf="https://catalogue.bnf.fr/ark:/12148/cb120550580"} et [Gumperz]{.personnalite idsp="Gumperz, John J." idbnf="https://catalogue.bnf.fr/ark:/12148/cb12130756s"}, la microsociologie de [Goffman]{.personnalite idsp="Goffman, Erving" idbnf="https://catalogue.bnf.fr/ark:/12148/cb11905281f"} et l'[ethnométhodologie]{.conceptEX idsp="ethnométhodologie" idbib="@garfinkel_studies_1967"} de [Garfinkel]{.personnalite idsp="Garfinkel, Harold" idbnf="https://catalogue.bnf.fr/ark:/12148/cb12046253v"}. [Goffman]{.personnalite idsp="Goffman, Erving" idbnf="https://catalogue.bnf.fr/ark:/12148/cb11905281f"} lui-même parlait d'éthologie de l'interaction.&nbsp;» [Cosnier dans @hotier_entretien_2001]. Il précise en outre que «&nbsp;dans ce type d'approche on ne part pas d'hypothèses, on y aboutit. \[...\] Cette démarche d'observation naturaliste n'empêche pas de pratiquer en complément des entretiens avec les [agents]{.conceptEX idsp="agent" idbib="@nadel_resonnance_2006"} et les usagers et de tenir compte de leur vécu.&nbsp;» C'est pourquoi il la nomme «&nbsp;éthologie compréhensive&nbsp;». C'est cette approche, permettant de croiser différents types de données (comportementales et issues d'entretiens par exemple), qui a été utilisée par [Jacques Cosnier]{.personnalite idsp="Cosnier, Jacques" idbnf="https://catalogue.bnf.fr/ark:/12148/cb11897776d"} et [Christine Develotte]{.personnalite idsp="Develotte, Christine" idorcid="https://orcid.org/0000-0002-1854-0422"} [-@cosnier_ethologie_2011] dans les premières recherches effectuées autour de la «&nbsp;conversation en ligne&nbsp;» [@develotte_decrire_2011].


## idParadoxeObservateur

---
title: >-
  Le paradoxe de l'observateur
credits: >-
  Séminaire IMPEC
keywords: paradoxe observateur,William Labov,sociolinguistique,observé,observateur
type: texte
lang: fr
link:
link-archive:
embed:
zotero:
date: 2020-12-01
source: auteur
priority: highpriority

session:
protagonist:
artefact:
interviewee:
interviewer:
transcriber:
transcription-link:
duration:
timecode:

---
La problématique de l'observation scientifique des comportements sociaux par le·la chercheur·e en sciences humaines n'est pas nouvelle. Le «&nbsp;paradoxe de l'observateur&nbsp;» a été pointé par [William Labov]{.personnalite idsp="Labov, William" idbnf="https://catalogue.bnf.fr/ark:/12148/cb122919173"} dès 1978 au cours de ses recherches sociolinguistiques&nbsp;: il s'agit de «&nbsp;chercher à observer le langage que parlent les gens quand on ne les observe pas&nbsp;» [@traverso_analyse_1999, 22]. Ce paradoxe repose sur la volonté de chaque chercheur·e de «&nbsp;restituer des éléments au plus près de la réalité vécue alors que cette réalité doit être soumise à l'artificiel de l'observation systématique&nbsp;» [@mouchon_a_1985, 2]. Les réflexions méthodologiques entreprises depuis lors aboutissent à deux possibilités principales de contournement de cette difficulté&nbsp;:

- soit le·la chercheur·e devient un membre de la communauté par une immersion et un temps d'observation long sur le terrain [@mouchon_a_1985, 2].

- soit l'observateur·rice et l'observé·e sont une seule et même personne --&nbsp;«&nbsp;le linguiste est aussi membre de la communauté observée, comme l'entreprit [Labov]{.personnalite idsp="Labov, William" idbnf="https://catalogue.bnf.fr/ark:/12148/cb122919173"} dans son étude du ghetto de Harlem&nbsp;» [@boutet_pratiques_2002].

C'est cette dernière solution qui a été retenue par notre groupe de recherche. Certains artefacts utilisés par les membres du groupe servent simultanément à la mise en présence des sujets entre eux et à la capture des données (par exemple les ordinateurs utilisés à la fois via les plateformes de visioconférence pour communiquer et via les logiciels de capture dynamique d'écran pour récolter les données interactionnelles). L'écran, servant tant à échanger en ligne pendant le séminaire qu'à capturer ces échanges pour les analyses futures, constitue alors autant un médium de communication que d'observation de cette même communication, réduisant ainsi le nombre d'artefacts utilisés simultanément.


## idEthnoVisuelle

---
title: >-
  L'ethnographie visuelle
credits: >-
  Séminaire IMPEC
keywords: ethnographie visuelle,ethnos,audio,visuel,
type: texte
lang: fr
link:
link-archive:
embed:
zotero:
date: 2020-12-01
source: auteur
priority: highpriority

session:
protagonist:
artefact:
interviewee:
interviewer:
transcriber:
transcription-link:
duration:
timecode:

---
L'ethnographie visuelle trouve son origine dans l'idée selon laquelle les pratiques sociales se manifestent au travers de symboles visibles incarnés dans les gestes, cérémonies, rituels et artefacts situés dans des environnements autant naturels que construits [@ruby_visual_1996, 1345].

Il est alors considéré, dès lors que les pratiques sociales se rendent visibles, que chaque chercheur·e doit être en mesure d'employer des technologies (audio)visuelles (photos, vidéos, etc.) afin de les recueillir et d'en constituer des données pouvant être exploitées, analysées, diffusées [@ruby_visual_1996, 1345]. L'image constitue un «&nbsp;élément intrinsèque et non extrinsèque du processus de recherche&nbsp;» en ethnographie visuelle [@dion_les_2007, 62]. Il s'agit d'une méthodologie heuristique cherchant à «&nbsp;conceptgraphier&nbsp;» (étudier et représenter) «&nbsp;l'ethnos&nbsp;» (culturalités, pratiques et relations sociales) par des données et supports (audio)visuels. Le support visuel, image fixe ou animée, se révèle être à la fois un outil et un objet de recherche [@dion_les_2007].

L'approche visuelle ne peut être une copie ou un substitut à l'ethnographie verbale mais doit développer une méthodologie et des objectifs alternatifs bénéficiant à l'anthropologie dans sa globalité [@macdougall_visual_1997, 292]. En portant l'attention sur des données (audio)visuelles, l'ethnographie visuelle propose de nouvelles modalités d'appréhension des individus, des relations sociales, des cultures matérielles et de la connaissance ethnographique elle-même [@pink_doing_2007, 22]. La méthodologie de recherche repose alors sur trois activités principales [@banks_rethinking_1997]&nbsp;:

- constituer des données (audio)visuelles (analyser les pratiques sociales en produisant des images)&nbsp;;

- examiner les données (audio)visuelles préexistantes (analyser les images fournissant des connaissances sur la société)&nbsp;;

- collaborer avec les acteur·rice·s sociaux·ales dans la production des données (audio)visuelles.


## idRecipientDesignPrinciple

---
title:  >-
  *Recipient design principle*
credits: >-
  Séminaire IMPEC
keywords: recipient design principle,Catherine Kerbrat-Orecchioni,interaction,Harvey Sacks,Emanuel A. Schegloff,Gail Jefferson
type: texte
lang: fr
link:
link-archive:
embed:
zotero:
date: 2020-12-01
source: auteur
priority: highpriority

session:
protagonist:
artefact:
interviewee:
interviewer:
transcriber:
transcription-link:
duration:
timecode:

---
[Catherine Kerbrat-Orecchioni]{.personnalite idsp="Kerbrat-Orecchioni, Catherine" idbnf="https://catalogue.bnf.fr/ark:/12148/cb119096042"} précise, quant à elle, que pour qualifier une situation d'interaction «&nbsp;il faut et il suffit que l'on ait un groupe de participants modifiable mais sans rupture, qui dans un cadre spatio-temporel modifiable mais sans rupture, parlent d'un objet modifiable mais sans rupture&nbsp;» [-@kerbrat-orecchioni_les_1990, 216].

Aussi, de sorte à garantir la continuité et la bonne conduite de l'interaction, l'activité de parole implique-t-elle nécessairement une adaptation à son auditoire correspondant au *recipient design principle*. Ce concept sous-entend que «&nbsp;tout au long de son travail de production l'émetteur tient compte projectivement de l'interprétation qu'il suppose que l'auditeur va faire de ses propos&nbsp;» [@kerbrat-orecchioni_discours_2005, 16]. En développant cette notion de *recipient design*, [Harvey Sacks]{.personnalite idsp="Sacks, Harvey" idbnf="https://catalogue.bnf.fr/ark:/12148/cb12949921f"}, [Emanuel A. Schegloff]{.personnalite idsp="Schegloff, Emanuel A." idwiki="https://www.wikidata.org/wiki/Q3051968"} et [Gail Jefferson]{.personnalite idsp="Jefferson, Gail" idbnf="https://catalogue.bnf.fr/ark:/12148/cb12949922s"} font référence aux multiples ressources, visibles dans le tour de parole d'un·e locuteur·rice, qui témoignent d'une orientation manifeste vers les coparticipant·e·s. Ce procédé s'inscrit dans la sélection des unités lexicales et thématiques, dans la manière d'ordonner les séquences, et également dans les obligations et alternatives retenues pour ouvrir et clore une interaction [@sacks_simplest_1974, 727]. Le principe de *recipient design* permet aux interactant·e·s de structurer leurs ressources langagières de manière à créer un [foyer d'attention]{.conceptEX idsp="foyer d'attention"} conversationnel commun, construire et contrôler conjointement le cours de l'interaction, garantir l'intelligibilité des éléments qui leur semblent pertinents et préserver la stabilité du lien interactionnel [@sacks_simplest_1974, 727].


## idDecrireConversationLigne

---
title: >-
  *Décrire la conversation en ligne*
credits: >-
  Séminaire IMPEC
keywords: décrire la conversation en ligne,Christine Develotte,interaction,multimodalité,plurisémioticité
type: texte
lang: fr
link:
link-archive:
embed:
zotero:
date: 2020-12-01
source: auteur
priority: highpriority

session:
protagonist:
artefact:
interviewee:
interviewer:
transcriber:
transcription-link:
duration:
timecode:

---
Les contributeur·rice·s à l'ouvrage *Décrire la conversation en ligne* [@develotte_decrire_2011] s'inscrivaient déjà dans un renouvellement des analyses traditionnelles centrées sur le texte, en proposant d'identifier et d'adapter les méthodes d'analyse des interactions en intégrant leur multimodalité --&nbsp;voco-posturo-mimo-gestualité&nbsp;-- et leur [plurisémioticité]{.conceptEX idsp="plurisémioticité"} --&nbsp;notamment le graphisme, l'audio, la vidéo. La présente recherche émerge de ces fondements et cherche à appréhender de manière interdisciplinaire les expériences écraniques à partir de comportements multimodaux et [plurisémiotiques]{.conceptEX idsp="plurisémioticité"} que nous avons rendus observables (captures d'écran dynamiques, enregistrements vidéos, etc.).


## idCitationNumeriqueBourassa

---
title: >-
  Le numérique de Renée Bourassa
credits: >-
  Séminaire IMPEC
keywords: citation numérique,René Bourassa
type: texte
lang: fr
link:
link-archive:
embed:
zotero:
date: 2020-12-01
source: auteur
priority: lowpriority

session:
protagonist:
artefact:
interviewee:
interviewer:
transcriber:
transcription-link:
duration:
timecode:

---
> Le numérique ne s'isole pas sur lui-même, il s'imbrique dans le monde physique de façon irréductible, et de façon tout aussi matérielle [@bourassa_design_2018].


## idSchemaDispositif

---
title: >-
  Schémas du dispositif
credits: >-
  Séminaire IMPEC
keywords: schema dispositif,chronotope
type: texte
lang: fr
link:
link-archive:
embed:
zotero:
date: 2020-12-01
source: auteur
priority: highpriority

session:
protagonist:
artefact:
interviewee:
interviewer:
transcriber:
transcription-link:
duration:
timecode:

---
Le premier schéma illustre le dispositif de communication utilisé dans ce séminaire hybride&nbsp;: des participant·e·s en présentiel à Lyon et d'autres à distance, via différents artefacts.

![Figure 1&nbsp;: Dispositif de communication](./media/c1Fig1DispositifCommunication.png)

Le deuxième schéma précise le dispositif de captation retenu pour la collecte des données.

![Figure 2&nbsp;: Dispositif de captation](./media/c1Fig2DispositifTechniqueCaptation.png)

Le troisième illustre l'expérience phénoménologique du séminaire en fonction des différents points de vue des participant·e·s.

![Figure 3&nbsp;: Salle du séminaire à Lyon durant la première séance, telle qu'elle est perçue par Amélie à Caen](./media/c1Fig3ChronotopeSeance1Amelie.png)

Le processus à l'œuvre dans la mise en présence des sujets est nommé ici «&nbsp;chronotope&nbsp;» à la suite des travaux littéraires de [Mikhaïl Bakhtine]{.personnalite idsp="Bakhtine, Mikhaïl" idwiki="https://www.wikidata.org/wiki/Q185375"} [-@bakhtine_esthetique_1978] faisant référence à la construction d'une forme d'unité de temps et de lieu. Ici le chronotope se construit au sein d'un cadre spatio-temporel hybride (physico-numérique), réticulaire (réseau de participant·e·s, de lieu et d'artefacts communicationnels) et progressif&nbsp;: le passage du lieu objectif (une salle quelconque) au lieu subjectif (ma connexion sur mon ordinateur et un ou des logiciels dédiés) au lieu intersubjectif (la perception des autres participant·e·s via mes artefacts).


## idExemplesChronotopes

---
title: >-
  Exemples de chronotopes
credits: >-
  Séminaire IMPEC
keywords: schema dispositif,chronotope
type: texte
lang: fr
link:
link-archive:
embed:
zotero:
date: 2020-12-01
source: auteur
priority: lowpriority

session:
protagonist:
artefact:
interviewee:
interviewer:
transcriber:
transcription-link:
duration:
timecode:

---

![Figure 4 : Séminaire, séance 2, point de vue de Samira](./media/c1ChronotopeSeance2Samira.png){.vignette}   

![Figure 5 : Séminaire, séance 3, point de vue de Tatiana](./media/c1ChronotopeSeance3Tatiana.png){.vignette}     

![Figure 6 : Séminaire, séance 4, point de vue de Christelle](./media/c1ChronotopeSeance4Christelle.png){.vignette}      

![Figure 7 : Séminaire, séance 5, point de vue de Jean-François](./media/c1ChronotopeSeance5Jeanfrancois.png){.vignette}   


## idDispositifArtefactPlateforme

---
title: >-
  Dispositif, artefact et plateforme
credits: >-
  Séminaire IMPEC
keywords: dispositif,artefact,plateforme
type: texte
lang: fr
link:
link-archive:
embed:
zotero:
date: 2020-12-01
source: auteur
priority: lowpriority

session:
protagonist:
artefact:
interviewee:
interviewer:
transcriber:
transcription-link:
duration:
timecode:

---
Par exemple, le dispositif de communication à distance s'appuie sur des plateformes de communication et de transfert de documents ([Adobe Connect]{.dispositif idsp="Adobe Connect" idwiki="https://www.wikidata.org/wiki/Q2551337"}, [Skype]{.dispositif idsp="Skype" idwiki="https://www.wikidata.org/wiki/Q40984"}, logiciel embarqué de [Beam]{.dispositif idsp="Beam" idwiki="https://www.wikidata.org/wiki/Q545834"}, Google Drive, etc.) ainsi que des artefacts hébergeant ces plateformes (ordinateurs, tablettes, [robot Kubi]{.dispositif idsp="Kubi"}, [robot Beam]{.dispositif idsp="Beam" idwiki="https://www.wikidata.org/wiki/Q545834"}, vidéoprojecteur, webcam mobile, etc.).

La notion d'«&nbsp;artefact&nbsp;» permet de distinguer l'humain du non humain et de désigner un objet non animé quel qu'il soit sans préciser sa fonction[^6]. Les artefacts (c'est-à-dire les objets physiques) sont donc distincts des plateformes (c'est-à-dire les logiciels intégrés dans ces artefacts)&nbsp;: [Skype]{.dispositif idsp="Skype" idwiki="https://www.wikidata.org/wiki/Q40984"} est une plateforme qui peut être utilisée sur différents artefacts --&nbsp;un ordinateur, une tablette, un téléphone, etc.

[^6]: En revanche le terme «&nbsp;outil&nbsp;» se fonde sur la fonction de l'objet, il permet de faire quelque chose qu'il n'est pas possible de faire sans, ou du moins il facilite l'action.


## deroulementDuSeminaire

---
title: >-
   Déroulement du séminaire
credits: >-
   Séminaire IMPEC
keywords: déroulement,séminaire,IMPEC,Présences numériques,Laboratoire ICAR
type: texte
lang: fr
link:
link-archive:
embed:
zotero:
date: 2020-12-01
source: auteur
priority: lowpriority

session:
protagonist:
artefact:
interviewee:
interviewer:
transcriber:
transcription-link:
duration:
timecode:

---
Le séminaire IMPEC, dans lequel a lieu l'atelier «&nbsp;Présences numériques&nbsp;», se déroule sur une journée entière, de façon mensuelle. Depuis 2016, les séances sont structurées en deux parties&nbsp;: une première partie consacrée au travail des doctorant·e·s ou à l'accueil de conférencier·e·s, la deuxième à l'avancement du projet. Si, dans la première partie, le séminaire est ouvert à tout le laboratoire ICAR, la seconde est réservée aux participant·e·s à l'étude. C'est donc sur une base quasi mensuelle que les échanges collectifs autour de cette recherche ont eu lieu, et continuent à avoir lieu au moment où s'écrivent ces lignes. Sur la douzaine de participant·e·s que comporte le groupe, environ un tiers (pas toujours les mêmes personnes) a suivi le séminaire à distance (de façon ponctuelle ou régulière).


## idChoixConf

---
title: >-
  Choix des conférences et des conférencier·e·s
credits: >-
  Séminaire IMPEC
keywords: choix,conférence,conférencier,data session
type: texte
lang: fr
link:
link-archive:
embed:
zotero:
date: 2020-12-01
source: auteur
priority: lowpriority

session:
protagonist:
artefact:
interviewee:
interviewer:
transcriber:
transcription-link:
duration:
timecode:

---
Chaque intervention (conférence ou *data session*) a été suivie de 45&nbsp;minutes d'interactions avec les participant·e·s en présentiel et à distance (faisant également partie du corpus). Les conférencier·e·s ont été choisis en fonction de la proximité de leur recherche avec la nôtre, en tant qu'ils étaient susceptibles de venir nourrir notre réflexion autour de la notion de «&nbsp;présences numériques&nbsp;».


## idEntretienExplicitation

---
title: >-
  Entretien d'explicitation
credits: >-
  Séminaire IMPEC
keywords: entretien,explicitation,Martinez
type: texte
lang: fr
link:
link-archive:
embed:
zotero:
date: 2020-12-01
source: auteur
priority: lowpriority

session:
protagonist:
artefact:
interviewee:
interviewer:
transcriber:
transcription-link:
duration:
timecode:

---
L'entretien d'explicitation consiste en une forme particulière d'entretien qui «&nbsp;s'intéresse au vécu de l'action, et plus précisément aux informations de type procédural, dans le but de reconstituer la structure de l'action&nbsp;» [@martinez_lentretien_1997, 2]. En d'autres termes, l'explicitation vise à faire décrire à la personne une action de la façon la plus fine possible et à intégrer les émotions, les pensées et les actions associées à la description. Le tout constituant (pour partie) la structure de l'action. Certains des entretiens ont été filmés afin d'appréhender la dimension multimodale de la parole (indices comportementaux, intonations de voix, gestes, etc.) de l'expérience subjective.


## idSeancesRetenues

---
title: >-
  Séances retenues
credits: >-
  Séminaire IMPEC
keywords: séance,choix,sélection
type: texte
lang: fr
link:
link-archive:
embed:
zotero:
date: 2020-12-01
source: auteur
priority: lowpriority

session:
protagonist:
artefact:
interviewee:
interviewer:
transcriber:
transcription-link:
duration:
timecode:

---
Voici les cinq séances retenues présentées synthétiquement ci-dessous et que nous commentons ci-après&nbsp;:


| Recueil 1 du 21.10.2016 | Recueil 2 du 18.11.2016| Recueil 3 du 20.01.2017| Recueil 4 du 24.03.2017| Recueil 5 du 28.04.2017|
|:-:|:-:|:-:|:-:|:-:|
| [Séance 1](https://hdl.handle.net/11403/impec/v2/2_corpus_presences_numeriques/presences_numeriques_161021_data_session_morgane/4_montages) </br>*Data session* de Morgane | [Séance 2](https://repository.ortolang.fr/api/content/impec/v2/2_CORPUS_Presences_Numeriques/Presences_Numeriques_161118_conference_anthropologues/4_Montages) </br>Conférence des Anthropologues | [Séance 3](https://hdl.handle.net/11403/impec/2_corpus_presences_numeriques/presences_numeriques_170120_travail_collectif/4_montages) </br>Travail collectif (Partie 1 et Partie 2) | [Séance 4](https://hdl.handle.net/11403/impec/2_corpus_presences_numeriques/presences_numeriques_170324_conference_susan_herring/4_montages) </br>Conférence de Susan Herring | [Séance 5](https://repository.ortolang.fr/api/content/impec/v2/2_CORPUS_Presences_Numeriques/Presences_Numeriques_170428_data_session_Christelle/4_Montages) </br>*Data session* de Christelle (Partie 1 et Partie 2) |   
</br>   
**Recueil 1**&nbsp;: [Morgane Domanchin]{.participant}, doctorante présente à Lyon, a exposé l'avancée de son travail de thèse (20&nbsp;minutes de présentation + 28&nbsp;minutes de discussion) au cours de sa présentation intitulée «&nbsp;Complexités dans les interactions pédagogiques par écran&nbsp;: le cas du *multitasking* chez les apprenants&nbsp;».

**Recueil 2**&nbsp;: [Évelyne Lasserre]{.personnalite idsp="Lasserre, Évelyne"} (Université Lyon&nbsp;1) et [Axel Guïoux]{.personnalite idsp="Guïoux, Axel" idorcid="https://orcid.org/0000-0003-2969-5398"} (Université Lyon&nbsp;2), présent·e·s à Lyon, ont donné une conférence intitulée «&nbsp;Mobilis Immobile - La présence au-delà de l'empêchement&nbsp;» (45&nbsp;minutes de présentation + 52&nbsp;minutes de temps de discussion).

**Recueil 3**&nbsp;: séance de travail (32&nbsp;minutes de présentation + 68&nbsp;minutes de temps de discussion) entre les participant·e·s à l'atelier «&nbsp;Présences numériques&nbsp;» à Lyon et à distance. Il s'agissait de déterminer quels axes de recherche seraient choisis par les différents sous-groupes. Chaque sous-groupe, à tour de rôle, a présenté ses idées qui ont été discutées collectivement de façon à articuler les différents axes entre eux.

**Recueil 4**&nbsp;: conférence distancielle de [Susan Herring]{.personnalite idsp="Herring, Susan" idorcid="https://orcid.org/0000-0001-7861-5206"} à San Diego (États-Unis) via le [robot Beam]{.dispositif idsp="Beam" idwiki="https://www.wikidata.org/wiki/Q545834"} et [Adobe Connect]{.dispositif idsp="Adobe Connect" idwiki="https://www.wikidata.org/wiki/Q2551337"} (50&nbsp;minutes de présentation + 50&nbsp;minutes de temps de discussion), «&nbsp;*Discourse Pragmatics of Robot Mediated Communication*&nbsp;».

**Recueil 5**&nbsp;: présentation distancielle de [Christelle Combe]{.personnalite idsp="Combe, Christelle"} (Aix-en-Provence) via le [Kubi]{.dispositif idsp="Kubi"} et [Adobe Connect]{.dispositif idsp="Adobe Connect" idwiki="https://www.wikidata.org/wiki/Q2551337"} (46&nbsp;minutes de présentation + 52&nbsp;minutes de temps de discussion), «&nbsp;De l'ethos imaginé à l'ethos produit par des apprentis-tuteurs en ligne&nbsp;». Cette présentation était suivie d'un temps de travail collectif.


## idCCC

---
title: >-
  Cellule de Corpus Complexes
credits: >-
  Séminaire IMPEC
keywords: Cellule de Corpus Complexes,laboratoire ICAR,Julien Gachet,Justine Lascar,Daniel Valero
type: texte
lang: fr
link:
link-archive:
embed:
zotero:
date: 2020-12-01
source: auteur
priority: lowpriority

session:
protagonist:
artefact:
interviewee:
interviewer:
transcriber:
transcription-link:
duration:
timecode:

---
On notera que les deux premières séances (septembre et octobre) ont été entièrement consacrées aux discussions en groupe sur le projet à mener et que la séance de février a également été dévolue à des échanges destinés à ajuster la suite du recueil de données.

Afin de réaliser ce recueil dans des conditions optimales, en particulier pour soigner la qualité des vidéos qui constituent la base de nos analyses, nous avons bénéficié de l'appui technique et méthodologique de la Cellule de Corpus Complexes liée au laboratoire ICAR. Cette structure d'appui à la recherche composée d'ingénieur·e·s d'études du CNRS --&nbsp;notamment pour ce projet [Julien Gachet]{.personnalite idsp="Gachet, Julien"}, [Justine Lascar]{.personnalite idsp="Lascar, Justine" idorcid="https://orcid.org/0000-0001-6951-1798"} et [Daniel Valero]{.personnalite idsp="Valero, Daniel"}&nbsp;-- nous a offert une assistance aux différentes étapes de la collecte de nos données[^9].

[^9]: Elle a aussi été d'une aide précieuse pour la partie postproduction, et plus précisément pour la valorisation de la production de la recherche.


## idCamTrepied

---
title: >-
  Vues des caméras sur trépied
credits: >-
  Séminaire IMPEC
keywords: mise en place,matériel,dispositif,séance
type: texte
lang: fr
link:
link-archive:
embed:
zotero:
date: 2020-12-01
source: auteur
priority: lowpriority

session:
protagonist:
artefact:
interviewee:
interviewer:
transcriber:
transcription-link:
duration:
timecode:

---

![Vue 1](./media/c1CamTrepiedV1.png)

![Vue 2](./media/c1CamTrepiedV2.png)


## idCamGoProVueSurplomb

---
title: >-
  Vue «&nbsp;en surplomb&nbsp;» de la caméra GoPro
credits: >-
  Séminaire IMPEC
keywords: mise en place,matériel,dispositif,séance
type: texte
lang: fr
link:
link-archive:
embed:
zotero:
date: 2020-12-01
source: auteur
priority: lowpriority

session:
protagonist:
artefact:
interviewee:
interviewer:
transcriber:
transcription-link:
duration:
timecode:

---

![](./media/c1CamGoProVueSurplomb.png)


## idCamV360

---
title: >-
  Vue de la caméra 360°
credits: >-
  Séminaire IMPEC
keywords: mise en place,matériel,dispositif,séance
type: texte
lang: fr
link:
link-archive:
embed:
zotero:
date: 2020-12-01
source: auteur
priority: lowpriority

session:
protagonist:
artefact:
interviewee:
interviewer:
transcriber:
transcription-link:
duration:
timecode:

---

![](./media/c1CamV360.png)


## idMiseEnPlaceVeille

---
title: >-
  Mise en place du matériel
credits: >-
  Séminaire IMPEC
keywords: mise en place,matériel,dispositif,séance
type: texte
lang: fr
link:
link-archive:
embed:
zotero:
date: 2020-12-01
source: auteur
priority: lowpriority

session:
protagonist:
artefact:
interviewee:
interviewer:
transcriber:
transcription-link:
duration:
timecode:

---
La mise en place de ce matériel commençait la veille de la séance de façon à mettre en ordre le dispositif et les fils électriques. Les captations elles-mêmes s'effectuaient une fois toutes les participantes à distance connectées, au début du séminaire scientifique proprement dit.

<!--\+ ajout de photos pour chacune des vues mentionnées-->

De façon à standardiser le référencement des données, nous avons adopté un système de dénomination des sessions et des vues qui fonctionne de la façon suivante :

* Séance&nbsp;1 - *Data session* de [Morgane]{.participant idsp="Morgane" iddescription="participants.html#chercheurMorgane"}

* Séance&nbsp;2 - Conférence des Anthropologues

* Séance&nbsp;3 - Travail collectif (Partie&nbsp;1 et Partie&nbsp;2)

* Séance&nbsp;4 - Conférence de [Susan Herring]{.personnalite idsp="Herring, Susan" idorcid="https://orcid.org/0000-0001-7861-5206"}

* Séance&nbsp;5 - *Data session* de [Christelle]{.participant idsp="Christelle" iddescription="participants.html#chercheurChristelle"} (Partie&nbsp;1 et Partie&nbsp;2)

Les extraits qui seront étudiés seront donc référencés par rapport à leur numéro de séance.


## idMontageVideo

---
title: >-
  Montage vidéo
credits: >-
  Séminaire IMPEC
keywords: montage,vidéo
type: texte
lang: fr
link:
link-archive:
embed:
zotero:
date: 2020-12-01
source: auteur
priority: highpriority

session:
protagonist:
artefact:
interviewee:
interviewer:
transcriber:
transcription-link:
duration:
timecode:

---
Par la suite, des montages vidéos bénéficiant d'un enrichissement sémiotique (incrustation de transcription verbale, de graphismes porteurs de sens analytique --&nbsp;flèches, cercles, etc.&nbsp;--, et zooms) ont été réalisés. Ce type de vidéo multimodale a la particularité de rendre [*accountable*]{.conceptEX idsp="*accountable*" idbib="@garfinkel_studies_1967, vii"} l'écologie globale de l'interaction et de mettre en saillance des microévénements interactionnels signifiants. La vidéo ne constitue donc pas une simple illustration du propos scientifique mais fait partie intégrante de l'argumentation scientifique&nbsp;; les analyses proposées dans cet ouvrage sont construites autour de la vidéo qui se fait à la fois source d'étude, processus analytique et démonstration de nouveaux concepts théoriques issus de ces analyses.

![Exemple de montage vidéo annoté](./media/c1VueAnnotee1.png)


## idSyntheseCorpus

---
title: >-
  Synthèse du corpus
credits: >-
  Séminaire IMPEC
keywords: corpus
type: texte
lang: fr
link:
link-archive:
embed:
zotero:
date: 2020-12-01
source: auteur
priority: lowpriority

session:
protagonist:
artefact:
interviewee:
interviewer:
transcriber:
transcription-link:
duration:
timecode:

---
Le tableau ci-dessous synthétise les données du corpus. Il comptabilise la durée des enregistrements, les pistes vidéos et audios, ainsi que les artefacts mobilisés pour chacune des séances.

| **Séance 1 </br>*Data session* de Morgane **| **Séance 2 </br>Conférence des Anthropologues** | **Séance 3 </br>Travail collectif** (partie 1 partie 2) | **Séance 4 </br>Conférence de Susan Herring** | **Séance 5 </br>*Data session* de Christelle** (partie 1 partie 2) |
|:-:|:-:|:-:|:-:|:-:|
| 00:48:44 | 01:37:00 | (1) 00:32:43 (2) 01:08:00 | 01:40:00 | (1) 00:46:49 (2) 00:52:57 |
| 8 pistes vidéo | 7 pistes vidéo | 7 pistes vidéo | 7 pistes vidéo | 7 pistes vidéo |
| 4 pistes audio | 4 pistes audio | 4 pistes audio | 4 pistes audio | 4 pistes audio |
| Adobe Connect et robot Beam | Adobe Connect et robot Beam | Adobe Connect, robots Kubi et Beam | Adobe Connect, robots Kubi et Beam | Adobe Connect, robots Kubi et Beam |


## idSynopsisPepites

---
title: >-
  Synopsis et pépites
credits: >-
  Séminaire IMPEC
keywords: synopsis,pépite,time code,moment-clé
type: texte
lang: fr
link:
link-archive:
embed:
zotero:
date: 2020-12-01
source: auteur
priority: lowpriority

session:
protagonist:
artefact:
interviewee:
interviewer:
transcriber:
transcription-link:
duration:
timecode:

---
Plus précisément, il était demandé d'annoter le *time code* d'entrée et de fin des événements repérés comme étant intéressants dans son axe de recherche et d'inscrire un commentaire descriptif. Ces synopsis ont permis d'avoir une vue globale sur le recueil étudié et de croiser les annotations. Les trois sous-groupes de recherche (attention, [politesse]{.conceptEX idsp="politesse"} et [corporéité]{.conceptEX idsp="corporéité"}) ont ainsi repéré, dans l'ensemble des données, les cinq mêmes moments-clés qui synthétisaient différents aspects significatifs que nous avons appelés des «&nbsp;pépites&nbsp;». Le repérage de ces pépites et moments-clés a permis de cibler les objets de recherche de chaque groupe et a également ouvert les discussions sur la méthodologie à adopter pour les transcriptions.


## idDispositifDecisionnel

---
title: >-
  Dispositif décisionnel
credits: >-
  Séminaire IMPEC
keywords: dispositif décisionnel
type: texte
lang: fr
link:
link-archive:
embed:
zotero:
date: 2020-12-01
source: auteur
priority: lowpriority

session:
protagonist:
artefact:
interviewee:
interviewer:
transcriber:
transcription-link:
duration:
timecode:

---
Une fois la décision prise par l'ensemble du groupe présent lors de la séance de discussion, le corollaire a été de considérer que les participant·e·s absent·e·s à la discussion entérinaient les décisions prises par les autres, de façon à ne pas ralentir l'avancement du projet et à respecter le calendrier.

Lors des *data sessions*, des propositions de visualisation ont émergé, elles ont été améliorées collectivement et intégrées ([schéma du dispositif](chapitre1.html#idSchemaDispositif) de [Morgane]{.participant idsp="Morgane" iddescription="participants.html#chercheurMorgane"}, [chronotopes](chapitre1.html#idExemplesChronotopes) de [Samira]{.participant idsp="Samira" iddescription="participants.html#chercheurSamira"}, etc.) comme ressources communes au groupe (avec mention des auteur·rice·s concerné·e·s).


## idOrtolang

---
title: >-
  Ortolang
credits: >-
  Séminaire IMPEC
keywords: Ortolang,presentation,plateforme
type: texte
lang: fr
link:
link-archive:
embed:
zotero:
date: 2020-12-01
source: auteur
priority: lowpriority

session:
protagonist:
artefact:
interviewee:
interviewer:
transcriber:
transcription-link:
duration:
timecode:

---
La plateforme&nbsp;:

- permet, au travers d'une véritable mutualisation, à la recherche sur l'analyse, la modélisation et le traitement automatique de notre langue de se hisser au meilleur niveau international&nbsp;;

- facilite l'usage et le transfert des ressources et des outils mis en place au sein des laboratoires publics vers les partenaires industriels et les PME&nbsp;;

- valorise le français et les langues de France à travers un partage des connaissances sur notre langue accumulées par les laboratoires publics.


## idGoogleDrive

---
title: >-
  Google Drive
credits: >-
  Séminaire IMPEC
keywords: Google Drive,plateforme
type: texte
lang: fr
link:
link-archive:
embed:
zotero:
date: 2020-12-01
source: auteur
priority: highpriority

session:
protagonist:
artefact:
interviewee:
interviewer:
transcriber:
transcription-link:
duration:
timecode:

---
Conscient·e·s des problématiques éthiques que pose l'usage de plateformes privées (concernant notamment la protection des données), nous avons fait le choix de cet espace partagé de façon responsable et réfléchie. En effet, cette plateforme n'a été utilisée que pour des échanges de notes et d'informations non sensibles, le corpus de données étant quant à lui stocké sur Ortolang. C'est l'aspect pratique de Google Drive qui nous a incité·e·s à l'utiliser et le fait que nous étions issu·e·s de différentes universités dont aucune ne proposait, à l'époque[^14], l'équivalent au niveau institutionnel.

[^14]: D'autres solutions ont depuis été mises en place, comme par exemple [AMUbox](https://dosi.univ-amu.fr/amubox){link-archive="https://web.archive.org/web/20201104170747/https://dosi.univ-amu.fr/amubox"} à Aix-en-Provence.

## idChoixChapitres

---
title: >-
  Choix des différents chapitres
credits: >-
  Séminaire IMPEC
keywords: chapitre,choix,thématique,angle
type: texte
lang: fr
link:
link-archive:
embed:
zotero:
date: 2020-12-01
source: auteur
priority: lowpriority

session:
protagonist:
artefact:
interviewee:
interviewer:
transcriber:
transcription-link:
duration:
timecode:

---
En 2017, la mise en commun des idées concernant les différents angles d'étude à privilégier a permis, dans un premier temps, de dégager trois thématiques de départ&nbsp;: l'attention, la [corporéité]{.conceptEX idsp="corporéité"} et la [politesse]{.conceptEX idsp="politesse"}. La proposition a été faite aux participant·e·s de se regrouper autour de l'un de ces trois angles. En septembre 2018, la thématique de quatre nouveaux chapitres a été décidée&nbsp;: la comparaison des [effets de présence]{.conceptEX idsp="effet de présence"} par artefact, la co-construction d'une [intelligence collective]{.conceptEX idsp="intelligence collective" idbib="@levy_intelligence_1994 ; @levy_jeu_2003 ; @levy_cultiver_2016"}, la formation à la recherche et l'effet dynamique de groupe. De nouveaux groupes se sont constitués pour le travail en commun.

Le choix des auteur·rice·s s’est effectué librement et le travail d’analyse s’est donc réalisé en sous-groupes. L’idée étant que chaque sous-groupe puisse convoquer un angle d’étude original et que chaque chapitre soit susceptible de mettre en lumière un aspect vu sous un certain angle, qui ne serait pas celui de l’ouvrage dans son ensemble.


## idEthique

---
title: >-
  Aspects éthiques&nbsp;: les mises en danger
credits: >-
  Séminaire IMPEC
keywords: éthique,danger
type: texte
lang: fr
link:
link-archive:
embed:
zotero:
date: 2020-12-01
source: auteur
priority: highpriority

session:
protagonist:
artefact:
interviewee:
interviewer:
transcriber:
transcription-link:
duration:
timecode:

---
S'étudier en tant que groupe permet certes d'éviter les problèmes de droits à l'image et de refus d'autorisation d'utilisation des vidéos à des fins de recherche. Cela rend donc une telle étude faisable. Cependant l'exposition de soi qu'implique cette décision demande à être consentie par tous et toutes dans les différentes dimensions de la recherche car cet engagement engage l'image de chacun·e des participant·e·s dans la durée. Nous reviendrons de façon plus précise sur cet aspect [en conclusion de cet ouvrage](conclusion.html).


## idExpositionDeSoi

---
title: >-
  Exposition de soi
credits: >-
  Séminaire IMPEC
keywords: exposition,face
type: texte
lang: fr
link:
link-archive:
embed:
zotero:
date: 2020-12-01
source: auteur
priority: highpriority

session:
protagonist:
artefact:
interviewee:
interviewer:
transcriber:
transcription-link:
duration:
timecode:

---
L'exposition de soi est classique aujourd'hui dans les vidéos en ligne souvent associées aux conférences publiques. Elle modifie certaines stratégies discursives en fonction de l'image que l'on souhaite voir conservée de soi (correction du langage, humour, etc.). Mais ici la situation d'énonciation est bien plus compliquée qu'une situation de monologue contrôlé. La spontanéité qu'impose aux participant·e·s la [situation polyartefactée]{.conceptCR idsp="situation polyartefactée" idglossaire="glossaire.html#GlossaireCommunicationMultimodale"}, par exemple dans leurs réactions aux différents désordres susceptibles de venir troubler le cours des interactions, les amène à ne pas pouvoir contrôler leur [*ethos*]{.conceptEX idsp="*ethos*"} comme l'on pourrait souhaiter le faire. Si ce sont précisément ces difficultés de communication qui sont au cœur de notre étude, c’est une autre affaire que de se sentir dépassé·e·s pas les événements, empêtré·e·s dans une situation inattendue, tout en sachant que nos phases de désarroi seront conservées.


## idMiseAuJour

---
title: >-
  Mise au jour publique des données
credits: >-
  Séminaire IMPEC
keywords: public,données
type: texte
lang: fr
link:
link-archive:
embed:
zotero:
date: 2020-12-01
source: auteur
priority: lowpriority

session:
protagonist:
artefact:
interviewee:
interviewer:
transcriber:
transcription-link:
duration:
timecode:

---
Les échanges maladroits et laborieux des participant·e·s constituent le cœur même de la recherche et sont donc assumés. Cependant la perspective de savoir que le corps des participant·e·s va servir non seulement de données à l'étude que l'on va réaliser nous-mêmes, mais aussi de données à d'autres chercheur·e·s n'est pas anodine. À la pression de l'enregistrement des données qui est, pour les participant·e·s présent·e·s à Lyon, anticipé lors des séances de séminaires, s'ajoute la pression de savoir que ces données seront *in fine* accessibles à la communauté scientifique. En conséquence, un certain nombre d'ajustements se sont avérés nécessaires pour négocier à la fois la [face]{.conceptEX idsp="face"} des participant·e·s et l'accès des données à la communauté scientifique.


## ajustementsDecides

---
title: >-
   Ajustements décidés
credits: >-
   Séminaire IMPEC
keywords: ajustements
type: texte
lang: fr
link:
link-archive:
embed:
zotero:
date: 2020-12-01
source: auteur
priority: lowpriority

session:
protagonist:
artefact:
interviewee:
interviewer:
transcriber:
transcription-link:
duration:
timecode:

---
La décision de conserver le prénom réel des participant·e·s vient rompre une tradition méthodologique dans l'analyse des interactions en sciences humaines et sociales. Habituellement, seules des initiales, voire des substitutions de prénoms, sont de mises. Ici, la revendication d'une étude réflexive passait, nous semblait-il, par cette exposition tant auctoriale qu'en tant qu'objet de recherche.

De plus, la décision collectivement prise de ne pas recourir à ce procédé découlait du caractère artificiel que cette opération aurait détenu : nous aurions été de fait reconnu·e·s par nos collègues chercheur·e·s. L’association à des prénoms différents aurait donc compliqué inutilement les analyses. Nous avons donc retenu l'idée de mentionner les vrais prénoms.

Les entretiens ayant été effectués à l'intérieur de notre groupe et non par des personnes extérieures, les formes de langage, d'humour ou de niveau de langue des réponses sont adaptées à la proximité sociale des participant·e·s entre eux·elles. C'est pourquoi il a été convenu que les fichiers audios seraient utilisés par les seul·e·s participant·e·s au groupe de recherche et qu'ils seraient transcrits pour être rendus publics sous cette forme.

En outre, une fois transcrits, les entretiens ont été discutés en groupe et il a paru nécessaire, pour ceux et celles qui le souhaitaient, de revoir la transcription brute pour la «&nbsp;nettoyer&nbsp;», en complétant les phrases, en explicitant les implicites et les déictiques, bref, en les clarifiant avant de diffuser les propos publiquement.





## idAjustements

---
title: >-
  Ajustements décidés
credits: >-
  Séminaire IMPEC
keywords: ajustements
type: texte
lang: fr
link:
link-archive:
embed:
zotero:
date: 2020-12-01
source: auteur
priority: lowpriority

session:
protagonist:
artefact:
interviewee:
interviewer:
transcriber:
transcription-link:
duration:
timecode:

---
La décision de conserver le prénom réel des participant·e·s vient rompre une tradition méthodologique dans l'analyse des interactions en sciences humaines et sociales. Habituellement, seules des initiales, voire des substitutions de prénoms, sont de mises. Ici, la revendication d'une étude réflexive passait, nous semblait-il, par cette exposition tant auctoriale qu'en tant qu'objet de recherche.

De plus, la décision collectivement prise de ne pas recourir à ce procédé découlait du caractère artificiel que cette opération aurait détenu : nous aurions été de fait reconnu·e·s par nos collègues chercheur·e·s. L’association à des prénoms différents aurait donc compliqué inutilement les analyses. Nous avons donc retenu l'idée de mentionner les vrais prénoms.

Les entretiens ayant été effectués à l'intérieur de notre groupe et non par des personnes extérieures, les formes de langage, d'humour ou de niveau de langue des réponses sont adaptées à la proximité sociale des participant·e·s entre eux·elles. C'est pourquoi il a été convenu que les fichiers audios seraient utilisés par les seul·e·s participant·e·s au groupe de recherche et qu'ils seraient transcrits pour être rendus publics sous cette forme.

En outre, une fois transcrits, les entretiens ont été discutés en groupe et il a paru nécessaire, pour ceux et celles qui le souhaitaient, de revoir la transcription brute pour la « nettoyer », en complétant les phrases, en explicitant les implicites et les déictiques, bref, en les clarifiant avant de diffuser les propos publiquement.


## GlossaireEthologieReflexiveVisuelle

---
title: >-
   Définition de l'éthologie réflexive visuelle
credits: >-
   Christine Develotte et Samira Ibnelkaïd
keywords: définition, éthologie
type: texte
lang: fr
link:
link-archive:
embed:
zotero:
date: 2020-12-01
source: auteur
priority: lowpriority

session:
protagonist:
artefact:
interviewee:
interviewer:
transcriber:
transcription-link:
duration:
timecode:

---
*Christine Develotte et Samira Ibnelkaïd*

L’éthologie réflexive visuelle se fonde sur l’éthologie des communications humaines telle que l’a définie [Jacques Cosnier]{.personnalite idsp="Cosnier, Jacques" idbnf="https://catalogue.bnf.fr/ark:/12148/cb11897776d"} [-@cosnier_specificite_1978 ; -@cosnier_ethology_1986 ; -@cosnier_lethologie_1987]&nbsp;: une science de l’observation des comportements humains doublée d’une prise en compte des ressentis et affects des sujets observés. Cette éthologie dite «&nbsp;compréhensive&nbsp;» [Cosnier dans @hotier_entretien_2001] a été, ici, appliquée à nous-mêmes (à la fois participant·e·s produisant des données d’interaction spontanée et chercheur·e·s analysant ces données). C’est pourquoi nous qualifions cette approche de «&nbsp;réflexive&nbsp;». La notion de réflexivité, en sciences humaines, peut être définie comme «&nbsp;l’aptitude du sujet à envisager sa propre activité pour en analyser la genèse, les procédés ou les conséquences&nbsp;» [@bertucci_place_2009]. La posture réflexive induit la nécessité de développer tant une capacité d’introspection subjective (pouvoir observer ses propres pratiques) qu’un décentrement de sa propre perspective (porter un regard critique sur sa pratique et sa démarche). L’éthologie réflexive implique des bénéfices techniques pour les chercheur·e·s (accès au terrain, compréhension aisée des éléments déictiques, du contexte spatio-temporel, de l’histoire interactionnelle, etc.). Néanmoins cette approche engendre également des difficultés intersubjectives (exposition de soi, gestion du groupe, des relations interpersonnelles, des niveaux et modalités d’engagement, rigueur scientifique et «&nbsp;neutralité axiologique&nbsp;» [@weber_essai_1917], etc.). L’éthologie réflexive nécessite donc un engagement fort de la part du·de la chercheur·e impliqué·e.

Des outils peuvent faciliter la pratique réflexive en jouant un rôle de médiation entre les événements interactionnels et le vécu subjectif des «&nbsp;chercheur·e·s-participant·e·s&nbsp;»&nbsp;: c’est le cas notamment de la vidéo. Dans cette recherche, l’observation et l’analyse des pratiques s’appuie en effet sur des enregistrements vidéo des données interactionnelles (caméra numérique, capture d’écran dynamique, logiciels de montage vidéo, etc.). L’usage de la vidéo au sein de cette démarche éthologique réflexive, «&nbsp;visuelle&nbsp;» donc, est à mettre en lien avec les travaux de l’ethnographie visuelle [@pink_doing_2007 ; @dion_les_2007]. L’image, statique ou dynamique, se révèle être à la fois un outil et un objet de recherche [@dion_les_2007]. Dans cette logique, les supports vidéo ne sont pas seulement des illustrations mais constituent le matériau de recueil et d’analyse des données d’interaction, et sont également exploités sémiotiquement pour rendre compte des résultats de ces analyses. En effet, des procédés graphiques divers (montage multiplan, encerclage et fléchage, zoom, gif...) permettent de visualiser voire d’expliciter les analyses ou les concepts théoriques crées, et ce au sein même des interactions étudiées.

L’éthologie réflexive visuelle se veut être une approche transdisciplinaire plaçant la multimodalité, le vécu subjectif et la sensorialité au cœur de l’analyse des interactions, de l’observation à la diffusion scientifiques.
