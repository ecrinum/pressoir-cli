<!--
Additionnels.md
- id SchemaDispositif : visualisation des Chronotopes (CA)
- id SeancesRetenues ; liens vidéos et sites du séminaire dans tableau des séances
- id MiseEnPlaceVeille : "\+ ajout de photos pour chacune des vues mentionnées". Quelles photos ?
- id MontageVideo : "\+ ajout de photos avec montages". Quelles photos ?
-->

Le travail de recherche présenté dans cet ouvrage se fonde sur une approche interdisciplinaire des données d’interactions multimodales et [plurisémiotiques]{.conceptEX idsp="plurisémioticité"}. Le [séminaire polyartefacté]{.conceptCR idsp="séminaire doctoral polyartefacté" idglossaire="glossaire.html#GlossaireSeminaireDoctoralPolyartefacte"} ici analysé fait l’objet d’une étude multidimensionnelle nécessitant, selon notre approche, un accès audiovisuel aux séquences d’actions verbales et non verbales de mise en présence de l'ensemble des participant·e·s. Il s’agit d’adopter une démarche d’éthologie compréhensive [@cosnier_specificite_1978], à savoir une «&nbsp;observation directe de comportements vécus dans l'ici et le maintenant&nbsp;» [@cosnier_cinquante_2013, 258] prenant en compte les événements interactionnels autant que les affects et processus empathiques [@cosnier_cinquante_2013].

Nous présentons dans ce chapitre les fondements théoriques et méthodologiques qui sous-tendent le recueil, la sélection et l’analyse de ce corpus de données audiovisuelles et justifient l’intérêt porté à ce terrain de recherche menant à l’émergence de ce que nous nommons une «&nbsp;[éthologie réflexive visuelle]{.conceptCR idsp="éthologie réflexive visuelle" idglossaire="glossaire.html#GlossaireEthologieReflexiveVisuelle"}&nbsp;».

## Choix théorico-méthodologiques

### L'éthologie comme approche globale du terrain

Au sein de son laboratoire lyonnais[^1], [Jacques Cosnier]{.personnalite idsp="Cosnier, Jacques" idbnf="https://catalogue.bnf.fr/ark:/12148/cb11897776d"}[^2] a choisi l'éthologie pour décrire les situations de communication interpersonnelle [-@cosnier_specificite_1978 ; -@cosnier_ethology_1986 ; -@cosnier_lethologie_1987]. Se fondant sur une analyse descriptive des comportements humains, cette approche y associe également le point de vue des individus observés à partir d'entretiens.

[Cosnier]{.personnalite idsp="Cosnier, Jacques" idbnf="https://catalogue.bnf.fr/ark:/12148/cb11897776d"} a désigné cette approche naturaliste d'«&nbsp;éthologie compréhensive&nbsp;».

!contenuadd(./idEthologieReflexiveDev)


!contenuadd(./idEthologieReflexiveApp)


Reprenant cette perspective éthologique, nous avons cherché ici à développer une approche nouvelle, l'«&nbsp;[éthologie réflexive visuelle]{.conceptCR idsp="éthologie réflexive visuelle" idglossaire="glossaire.html#GlossaireEthologieReflexiveVisuelle"}&nbsp;», en ce sens qu’elle porte sur des données d’interactions vidéos et qu’elle est appliquée à nous-mêmes, intégrant par là les avantages et les limitations dues au fait que l’éthologue et son objet se confondent et que, par exemple, les entretiens ont été conduits entre nous.

Le fait de recueillir les données pour chacune des séances a bien entendu modifié l'environnement classique du séminaire en y ajoutant micros et caméras susceptibles, par leur présence, d'influer sur les comportements des participant·e·s. La recherche effectuée intègre ce facteur qui n'invalide pas l'approche naturaliste retenue qui s'effectue précisément via l'enregistrement des comportements.

!contenuadd(./idParadoxeObservateur)


Une telle démarche impose de faire des choix techniques éclairés, tels que le nombre de caméras et leur emplacement, qui sont issus des expertises acquises au sein du laboratoire ICAR. Cette [approche éthologique réflexive visuelle]{.conceptCR idsp="éthologie réflexive visuelle" idglossaire="glossaire.html#GlossaireEthologieReflexiveVisuelle"} prend place dans un paysage balisé en sciences humaines et sociales par l'ethnographie visuelle et l'analyse interactionnelle.

### L'ethnographie visuelle

La complexité de l'étude de la présence des sujets par écran requiert, selon nous, une approche multimodale et [plurisémiotique]{.conceptEX idsp="plurisémioticité"}. C'est pourquoi nous faisons appel au domaine de l'ethnographie visuelle [@ruby_visual_1996 ; @banks_rethinking_1997 ; @pink_doing_2007 ; @dion_les_2007] pour appréhender l'écologie globale de l'interaction physico-numérique et suivre la progression du flux de [présence transmédiatique]{.conceptEX idsp="présence transmédiatique"}, en faisant usage des outils digitaux à disposition de chaque chercheur·e en [humanités numériques]{.conceptEX idsp="humanités numériques"} (caméra numérique, capture d'écran dynamique, logiciels de montage vidéo, etc.). Cette approche nous permet d'étudier les comportements communicatifs autant verbaux que non verbaux, sur et hors écran, et nous amène à appréhender la présence par écran comme un phénomène langagier, sensoriel et technique.

!contenuadd(./idEthnoVisuelle)

Au sein des méthodes visuelles, l'enregistrement vidéo représente, plus qu'un outil de recueil de données, une technologie participant à la négociation des relations sociales[^3] et un média par lequel la connaissance ethnographique est produite [@pink_doing_2007, 173]. De surcroît, les nouvelles technologies numériques, les interfaces et les réseaux socionumériques introduisent progressivement des études ethnographiques portant sur les pratiques communicationnelles digitales quotidiennes des individus et des communautés [@pink_doing_2007, 197]. Émerge alors, au-delà de l'ethnographie visuelle, une ethnographie numérique se voulant délinéarisée, multimodale et [plurisémiotique]{.conceptEX idsp="plurisémioticité"} [@pink_doing_2007, 197].

### L'analyse interactionnelle

La notion d'interaction recouvre des définitions plus ou moins restreintes en fonction de l'attitude portée à son égard. [Goffman]{.personnalite idsp="Goffman, Erving" idbnf="https://catalogue.bnf.fr/ark:/12148/cb11905281f"}, linguiste et sociologue figurant parmi les fondateur·rice·s de l'analyse des interactions, explique que&nbsp;:

> Par interaction (c'est-à-dire l'interaction face à face), on entend à peu près l'influence réciproque que les partenaires exercent sur leurs actions respectives lorsqu'ils sont en présence physique immédiate les uns des autres [@goffman_presentation_1973, 23].

!contenuadd(./idRecipientDesignPrinciple)


L'ensemble des ressources conversationnelles nous renseigne alors sur l'activité que les participant·e·s façonnent depuis le tour de parole à la structure globale de l'interaction afin d'en définir le contenu, la forme et les modalités de présences mises en jeu. C'est donc notamment dans une orientation interactionniste à partir des travaux initiés par [Goffman]{.personnalite idsp="Goffman, Erving" idbnf="https://catalogue.bnf.fr/ark:/12148/cb11905281f"}, et par [Sacks]{.personnalite idsp="Sacks, Harvey" idbnf="https://catalogue.bnf.fr/ark:/12148/cb12949921f"}, [Schegloff]{.personnalite idsp="Schegloff, Emanuel A." idwiki="https://www.wikidata.org/wiki/Q3051968"} et [Jefferson]{.personnalite idsp="Jefferson, Gail" idbnf="https://catalogue.bnf.fr/ark:/12148/cb12949922s"}, puis poursuivis notamment par [Cosnier]{.personnalite idsp="Cosnier, Jacques" idbnf="https://catalogue.bnf.fr/ark:/12148/cb11897776d"}, [Kerbrat-Orecchioni]{.personnalite idsp="Kerbrat-Orecchioni, Catherine" idbnf="https://catalogue.bnf.fr/ark:/12148/cb119096042"}, [Véronique Traverso]{.personnalite idsp="Traverso, Véronique" idbnf="https://catalogue.bnf.fr/ark:/12148/cb125228530"} et [Lorenza Mondada]{.personnalite idsp="Mondada, Lorenza" idwiki="https://www.wikidata.org/wiki/Q1870158"} que certains des chapitres ici proposés analyseront les productions langagières des participant·e·s.

Il s'agit en outre, par les travaux ici regroupés, d'étendre cette approche interactionniste par l'étude de l'impact de l'écran sur les rituels interactionnels observés jusqu'alors hors écran. Il apparaît nécessaire de décrire «&nbsp;la frontière entre nouvelles pratiques et structures normatives, et l'appropriation par les acteurs humains à la fois des outils et des pratiques discursives ou sémiotiques qu'ils induisent&nbsp;» [@develotte_decrire_2011, 19].

!contenuadd(./idDecrireConversationLigne)


### Une approche transdisciplinaire&nbsp;: l’éthologie réflexive visuelle

Nous avons choisi de faire usage de la vidéo pour capturer, analyser et illustrer les phénomènes interactionnels[^4]. Il s’agit donc non pas de retranscrire les productions verbales en les accompagnant de mises en mots des gestes interactionnels --&nbsp;tel que le propose la tradition de l’analyse conversationnelle fondée au départ sur des enregistrements audio&nbsp;-- mais plutôt de conserver le matériau audiovisuel primaire et de guider le ou la lecteur·rice-observateur·rice par un enrichissement sémiotique et narratif réalisé en postproduction. La vidéo constitue ainsi un mode de représentation analytique en soi qui suit un scénario établi en amont par chaque chercheur·e. Les capsules vidéos comme illustrations dynamiques forment selon nous une modalité novatrice de restitution du travail d’analyse des données et participent du renouvellement de l’étude des interactions sociales en mettant à profit les outils technologiques à la disposition des chercheur·e·s en [humanités numériques]{.conceptEX idsp="humanités numériques"}.

Au-delà de ce cadre théorico-méthodologique général, les auteur·rice·s des différents chapitres de cet ouvrage ont choisi des cadres théorico-méthodologiques spécifiquement adaptés à leur thématique et exposés au sein de chaque chapitre. Le fait de convoquer différents domaines dans nos analyses implique que les mêmes concepts sont parfois utilisés différemment selon les approches choisies.

## Situation matérielle

Nous commencerons par décrire l'«&nbsp;[écosystème numérique]{.conceptEX idsp="écosystème numérique" idbib="@bourassa_design_2018"}&nbsp;» [@bourassa_design_2018] du séminaire, en termes matériels et humains. Ce concept permet de penser les contextes où entrent en jeu de multiples acteur·rice·s, qu'ils soient humains ou non humains, liés par des relations organiques, techniques et dynamiques.

!contenuadd(./idCitationNumeriqueBourassa)


Dans le cas de notre séminaire, l'intrication des dimensions présentielle et distancielle s'effectue en effet par le biais des outils et artefacts de communication.

### Sur le plan spatial

#### La salle du Laboratoire d'Innovation Pédagogique et Numérique (LiPeN)

Le séminaire «&nbsp;Interactions Multimodales Par ÉCrans&nbsp;» (IMPEC) se tient à l'École normale supérieure (ENS) de Lyon dans une salle adaptée au travail d'atelier pédagogique (espace ouvert, mobilier mobile et modulable)[^5].

!contenuadd(./idSchemaDispositif)

!contenuadd(./idExemplesChronotopes)


Lors des séances enregistrées, les participant·e·s à distance se trouvaient, selon les sessions[^40], à Londres (Royaume-Uni), Hangzhou (Chine), Besançon, Caen et Aix-en-Provence (France) et ont utilisé des artefacts variés pour communiquer.

#### Les artefacts de communication à distance et leur disposition dans la pièce

Dans cet ouvrage, nous différencions les notions de «&nbsp;dispositif&nbsp;», d'«&nbsp;artefact&nbsp;» et de «&nbsp;plateforme&nbsp;».
Nous entendons par dispositif l'agencement de multiples artefacts et l'usage de différentes plateformes visant à produire des formes de présence.

!contenuadd(./idDispositifArtefactPlateforme)


Le dispositif de recueil de données est quant à lui constitué des micros et des caméras permettant la capture des données d'interactions.

![Figure 8&nbsp;: Robot Beam](./media/c1Fig4Beam.jpg)

![Figure 9&nbsp;: Kubi](./media/c1Fig5Kubi.png)


### Sur le plan temporel

Le programme général de la recherche a été établi ainsi&nbsp;:

- 2016-2017 -- design de la recherche et recueil des données&nbsp;;
- 2017-2018 -- traitement des données, archivage, choix des axes de recherche, début de discussion concernant l'éditorialisation&nbsp;;
- 2018-2019 -- analyse des données et écriture du livre, réflexions préparatoires à l'édition en ligne&nbsp;;
- 2019-2020 -- éditorialisation en ligne et ouverture de [la base Ortolang contenant le corpus des données de cette recherche](https://www.ortolang.fr/market/corpora/impec/v1){link-archive="https://web.archive.org/web/20201104163300/https://www.ortolang.fr/market/corpora/impec/v1"} au public scientifique.

!contenuadd(./deroulementDuSeminaire)


## Deux types de données

Les données qui ont été recueillies sont de deux types&nbsp;: des données comportementales et des données issues des entretiens avec les participant·e·s.

### Données comportementales

Lors de l'année 2016-2017, le choix de cinq séances de séminaire (cf. [ci-dessous](chapitre1.html#idSeancesRetenues)) s'est effectué en fonction de la variation maximale des situations de communication&nbsp;: nous avons cherché à ce que les conférencier·e·s se trouvent tour à tour en situation présentielle et distancielle (à distance via [robot Beam]{.dispositif idsp="Beam" idwiki="https://www.wikidata.org/wiki/Q545834"} ou [Kubi]{.dispositif idsp="Kubi"} ou, en présentiel, à Lyon), de façon à multiplier les scénarios de communication à étudier[^7]. Nous avons également cherché à varier différents critères tels que le statut de chaque conférencier·e (doctorant·e ou chercheur·e confirmé·e).

!contenuadd(./idChoixConf)


### Données relatives au ressenti des participant·e·s

Il s'agit principalement d'entretiens filmés ou enregistrés qui ont été méthodologiquement différents selon l'optique de recherche&nbsp;: entretien d'explicitation [@vermersch_entretien_1994] ou bien semi-dirigé pour clarifier des aspects précis.


!contenuadd(./idEntretienExplicitation)


D'autres recueils des perceptions individuelles ont eu lieu par écrit ; tant à la fin de chaque séance, en vue d'une évaluation du ressenti de la présence des un·e·s et des autres, que de manière asynchrone, donc plus réfléchis, en aval des séminaires ([quatre questions relatives au ressenti des participant·e·s](https://hdl.handle.net/11403/impec/v2/2_corpus_presences_numeriques/presences_numeriques_questionnaires_bilan )). Par ailleurs, [18 entretiens](https://hdl.handle.net/11403/impec/v2/2_corpus_presences_numeriques/presences_numeriques_transcriptions_entretiens) sous forme audio ou vidéo ont fait l'objet d'une transcription qui a été relue par la personne concernée[^8].


## Dispositif de recueil des données

!contenuadd(./idSeancesRetenues)


Ce sont cinq séances sur les dix de l'année 2016-2017 qui ont été sélectionnées pour constituer le corpus de recherche d'une durée totale de 9 heures 16 minutes. Chaque séance a été filmée, à Lyon, sous trois à quatre angles différents, et deux à quatre prises de son différentes. Par ailleurs, au moins deux vidéos ont été recueillies à chaque séance pour documenter les comportements des participant·e·s distanciel·le·s par le biais de captures dynamiques d'écran ou de vidéos externes. Ces données peuvent être disposées en multiécran et, selon les analyses, proposer des agrandissements de certains aspects par l'utilisation du zoom[^41].

!contenuadd(./idCCC)


### Recueil de données

Le premier aspect a concerné le repérage des lieux et le choix des matériels de recueil de données (micros, webcam et caméras) de façon à ce que les prises de vues soient le plus riches possible par rapport à nos objectifs de recherche.

Ainsi&nbsp;:

- entre deux et trois caméras fixes sur trépieds ont été disposées autour de la pièce pour permettre à la fois une vue globale et une vue centrée sur le diaporama projeté,

!contenuadd(./idCamTrepied)

- une caméra action GoPro a été utilisée pour rendre compte d'une vue «&nbsp;en surplomb&nbsp;» sur la salle. Cette même caméra a parfois été placée devant le [Kubi]{.dispositif idsp="Kubi"} lorsque celui-ci était utilisé,

!contenuadd(./idCamGoProVueSurplomb)

- une caméra 360° a été disposée au centre,

!contenuadd(./idCamV360)

- quatre micros ont été répartis autour de la table en fer à cheval.

!contenuadd(./idMiseEnPlaceVeille)


### Travail de postproduction

À l'issue de chaque récolte de données, chaque source (audio, vidéo, capture d'écran, vue à distance) était traitée et synchronisée sur la base d'une même échelle temporelle (également appelée *time code*).

Cette synchronisation contribue à faciliter et enrichir l'analyse de phénomènes en permettant l'intégration de différents points de vue (*in situ* et *ex situ*). Par la suite, des montages audios et vidéos ont été réalisés par le biais des logiciels Final Cut Pro X et QuickTime Pro. Il s'agit de montages multiscopes (plusieurs angles de vue sur un même écran) dans lesquels des vues étaient sélectionnées permettant de combiner simultanément six à huit vues. Pendant la réalisation de ces montages, les sources audios ont été intégrées aux fichiers vidéos[^10], pour permettre une meilleure répartition du son. Ces premiers montages ont constitué une base sur laquelle les sous-groupes de recherche ont pu s'appuyer pour illustrer leurs analyses.

!contenuadd(./idMontageVideo)


!contenuadd(./idSyntheseCorpus)

Au total, le corpus [«&nbsp;Présences numériques&nbsp;»](https://hdl.handle.net/11403/impec/v2/2_corpus_presences_numeriques) compte&nbsp;:

- 7 heures d'enregistrement vidéo (durée des cinq séances),

- 35 heures d'enregistrement vidéo toutes vues comprises,

- 10 heures de captures d'écran,

- 28 heures d'audio.

### Stockage

Le stockage s'est effectué sur la base de données Ortolang qui sera présentée [plus loin](chapitre1.html#outils-de-partage-des-documents). Afin de simplifier le partage entre nous, les données son et vidéos numérisées ont été classées et répertoriées selon une nomenclature permettant de les repérer facilement puis rangées dans des dossiers associés à chacune des cinq séances «&nbsp;IMPEC\_LiPeN-année-mois-jour&nbsp;». Une fiche technique récapitulative comprenant une rapide description de l'ensemble des vues disponibles accompagne chacun des recueils de données.

### Élaboration de synopsis et mise en place d'un espace de travail collectif

Au cours de nos rencontres, nous avons cherché à mettre en place une méthodologie efficace permettant l'annotation collective. Ainsi nous avons procédé à la création de fichiers «&nbsp;synopsis&nbsp;» au sein d'espaces de travail[^11] accessibles à l'ensemble du groupe et dans lesquels il a été demandé à chacun·e de renseigner des événements particulièrement pertinents en fonction de son axe de recherche.

!contenuadd(./idSynopsisPepites)


### Transcriptions

Dix-huit entretiens (audios et vidéos) ont été menés avec chaque participant·e du groupe de recherche et ont été transcrits très simplement, c’est-à-dire en prenant uniquement en compte la partie verbale. Ces 14 heures 36 minutes audios ont été utilisées par le groupe et c’est la version transcrite de ces entretiens qui est rendue publique. Un livret de transcription de 231 pages, dans lequel les transcriptions ont été classées par ordre chronologique, a été réalisé et distribué, en mai 2019, aux différent·e·s participant·e·s.

## Dispositif décisionnel et organisation des échanges

### Choix d'animation du séminaire

En dehors du rôle d'animatrice du séminaire évoqué par [Christine]{.participant idsp="Christine" iddescription="participants.html#chercheurChristine"} dans l'[introduction](introduction.html), d'autres rôles ont été attribués et d'autres encore se sont imposés spontanément au cours des séances. L'organisation du séminaire, d'un point de vue logistique comme technique, a ainsi été largement assurée par ses membres.

Par exemple, [Morgane]{.participant idsp="Morgane" iddescription="participants.html#chercheurMorgane"}, doctorante à Lyon et très impliquée dans la vie du laboratoire ICAR, a été chargée d'assister la mise en place matérielle de la salle en relation avec les membres de la Cellule Corpus Complexes. Elle a également assuré le suivi de la numérisation des vidéos et transcrit les entretiens. En dehors de cet appui technique et méthodologique «&nbsp;officiel&nbsp;», d'autres assistances ont aidé à la bonne marche des opérations[^12].


### Avancement collaboratif du travail d'analyse

Le choix a été fait d'associer l'ensemble des participant·e·s à toutes les étapes de la recherche. Cette implication de tou·te·s dans une co-construction de la recherche a impliqué une politique de concertation concernant les multiples tâches et sous-tâches composant cette recherche. Les discussions concernant chacune des décisions à prendre ont pris la forme d'ateliers de réflexion ou bien elles ont surgi au détour d'une *data session*. Les avis de chacun·e ont également pu être sollicités par email entre les séances. La mise en discussion a concerné, par exemple, le choix de la place des caméras dans la salle à Lyon, l'anonymisation ou non des données pour les publications...

!contenuadd(./idDispositifDecisionnel)


### Outils de partage des documents

#### Une plateforme de dépôt scientifique

La plateforme [Ortolang](https://www.ortolang.fr/){link-archive="https://web.archive.org/web/20201104165118/https://www.ortolang.fr/"} est un équipement d'excellence spécialisé pour la langue, complémentaire de l'offre générale proposée par [Huma-Num](http://www.huma-num.fr/){link-archive="https://web.archive.org/web/20201104165254/https://www.huma-num.fr/"}. Son but est de proposer une infrastructure en réseau offrant un réservoir de données (corpus, lexiques, dictionnaires, etc.) et d'outils sur la langue et son traitement clairement disponibles et documentés.

!contenuadd(./idOrtolang)


Cette plateforme héberge nos données depuis le début de nos recherches. Elle a été choisie pour sa simplicité d'utilisation, son interface étant très conviviale, pour ses grandes capacités de stockage et pour la possibilité qu'elle offre d'ouvrir *in fine* le corpus à différents publics, de celui des seul·e·s chercheur·e·s au grand public[^13]. Cet aspect *open data* renvoie au choix de politique scientifique effectué pour ce projet.

#### Un espace partagé pour données non sensibles

Nous avons utilisé Google Drive pour le stockage ponctuel de données associées à la recherche (préparation de publications, prévision de colloques, extraits vidéos...). Des Google Docs ont été utilisés pour les prises de notes collectives durant les séminaires, ces notes étant de même archivées sur Google Drive. Nous avons également procédé à l'élaboration collective de synopsis associés à chacune des séances.

!contenuadd(./idGoogleDrive)


## Dispositif de rédaction

L'idée était de rendre compte de l'expérience vécue par le groupe de façon diffractée, à partir de la mise en lumière de différents aspects qui nous paraissaient les plus intéressants à étudier dans un premier temps.


!contenuadd(./idChoixChapitres)



### Travail de co-construction de l'ensemble

Le fonctionnement adopté fut le suivant&nbsp;: durant le séminaire, chacun des sous-groupes devait présenter la façon dont il pensait aborder les analyses, l'angle théorico-méthodologique choisi et quelques exemples de données exploitables. Chaque présentation a donné lieu à de nombreux échanges avec l'ensemble du groupe permettant d'éclaircir certains points et d'en enrichir d'autres.

Ces retours du groupe tout au long de l'écriture ont pris la forme d'un atelier de recherche de deux jours en juin 2019, centré sur les premières versions des différents chapitres.


## Spécificités d'une recherche réflexive

Le fait de se prendre soi-même comme objet d'étude sur un sujet, l'étude des [présences numériques]{.conceptEX idsp="présence numérique"}, c'est-à-dire la communication multimodale en situation hybride qui constitue le domaine d'expertise des (apprenti·e·s-)chercheur·e·s, n'a rien d'anodin et engendre des effets qu'il importe d'intégrer dans l'analyse.

### Réflexivité par rapport à l'objet de recherche

Les effets induits par la familiarité avec le sujet amènent par exemple à penser que les résultats des recherches se rapportent à un public «&nbsp;non naïf&nbsp;» susceptible donc d'adopter des comportements plus adaptés (dans le positionnement par rapport à la webcam ou l'utilisation du *chat* sur [Adobe Connect]{.dispositif idsp="Adobe Connect" idwiki="https://www.wikidata.org/wiki/Q2551337"} par exemple) qu'un public non averti.

Par ailleurs, la neutralité des intervieweur·se·s devient toute relative quand il s'agit de collègues proches engagé·e·s dans la même aventure que soi-même, et l'on peut faire l'hypothèse que la préservation des [faces]{.conceptEX idsp="face"} des un·e·s et des autres peut être plus grande, surtout lorsque l'on sait que tout ce qui est dit sera rendu public. La bonne relation socioaffective entre les membres du groupe est une dimension assumée par rapport aux opinions recueillies.

!contenuadd(./idEthique)

!contenuadd(./idExpositionDeSoi)

!contenuadd(./idMiseAuJour)

!contenuadd(./ajustementsDecides)


## Vers une «&nbsp;éthologie visuelle&nbsp;»

Par les spécificités théoriques et méthodologiques qui viennent d’être présentées, nous visons à poser les bases de ce que nous proposons de nommer une [approche éthologique visuelle]{.conceptCR idsp="éthologie réflexive visuelle" idglossaire="glossaire.html#GlossaireEthologieReflexiveVisuelle"}. Elle s’appuie sur la vidéo et aussi largement sur la subjectivité de l'expérience vécue par les participant·e·s.

!contenuadd(./GlossaireEthologieReflexiveVisuelle)


[^1]: Laboratoire d'éthologie des communications, au fondement de l'actuel [laboratoire ICAR](http://icar.cnrs.fr/){link-archive="https://web.archive.org/web/20201104165621/http://icar.cnrs.fr/"}.

[^2]: [Jacques Cosnier]{.personnalite idsp="Cosnier, Jacques" idbnf="https://catalogue.bnf.fr/ark:/12148/cb11897776d"} a une formation initiale de biologiste.

[^3]: L'usage de la technologie sur le terrain ne peut se réaliser qu'à travers une [coopération]{.conceptEX idsp="coopération"} éclairée et des négociations explicites avec les participant·e·s afin de nouer une relation de confiance indispensable à une constitution éthique du corpus de données interactionnelles.

[^4]: Complétés par des entretiens semi-guidés et d’explicitation.

[^5]: Cet espace porte le nom de «&nbsp;Laboratoire d'Innovation Pédagogique et Numérique&nbsp;» (LIPeN).


[^7]: Cf. [«&nbsp;Les spécificités d’une recherche réflexive&nbsp;»](chapitre1.html#spécificités-dune-recherche-réflexive).

[^8]: Pour les participant·e·s, la multiplicité des expériences n'a souvent pas été possible. Seules quelques-unes ont pu expérimenter l'utilisation de tous les artefacts de communication à distance.


[^10]: C'est ainsi que, contrairement au fichier .mp4, les fichiers .mov permettent aux chercheur·e·s, à partir du logiciel QuickTime Pro, de cocher ou décocher une piste audio. En cas de chevauchement entre participant·e·s par exemple, cette fonctionnalité permet de mettre en silence l'une des pistes son décochée, et peut s'avérer utile à la transcription des tours de parole.

[^11]: Google Sheets sur Google Drive.

[^12]: [Dorothée]{.participant idsp="Dorothée" iddescription="participants.html#chercheurDorothee"} s'est occupée du [robot Beam]{.dispositif idsp="Beam" idwiki="https://www.wikidata.org/wiki/Q545834"}, [Christelle]{.participant idsp="Christelle" iddescription="participants.html#chercheurChristelle"} a mis en place les séances de visioconférence sur [Adobe Connect]{.dispositif idsp="Adobe Connect" idwiki="https://www.wikidata.org/wiki/Q2551337"} et a permis le lien avec l'équipe d'Ortolang pour le stockage des données, [Caroline]{.participant idsp="Caroline" iddescription="participants.html#chercheurCaroline"} a pris en charge les réservations des salles et du matériel, les prises de note Google Docs et la gestion du Google Drive.

[^13]: Voir le corpus [«&nbsp;Présences numériques&nbsp;»](https://www.ortolang.fr/market/corpora/impec){link-archive="https://web.archive.org/web/20201104170626/https://www.ortolang.fr/market/corpora/impec"} sur la plateforme Ortolang.

[^40]: Cf. [Introduction](introduction.html).

[^41]: Voir les photos dans l'annexe [«&nbsp;Enjeux techniques et défis méthodologiques de l’ingénierie de terrain au service de la recherche&nbsp;»](annexe.html).


## Références
