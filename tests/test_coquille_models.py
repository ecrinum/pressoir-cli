def test_titles(coquille):
    assert coquille.title == "Titre du livre test"
    assert coquille.title_h == "Titre du livre test"
    assert coquille.subtitle == "Sous-titre du <em>livre test</em>"
    assert coquille.subtitle_f == "Sous-titre du livre test"


def test_authors(coquille):
    assert len(coquille.authors) == 2
    assert [f"{author.forname} {author.surname}" for author in coquille.authors] == [
        "Prénom Auteur 1 Nom Auteur 1",
        "Prénom Auteur 2 Nom Auteur 2",
    ]


def test_chapters(coquille):
    assert len(coquille.chapters) == 9
    assert [chapter.id for chapter in coquille.chapters] == [
        "introduction",
        "chapitre1",
        "chapitre2",
        "chapitre3",
        "chapitre4",
        "chapitre5",
        "conclusion",
        "bibliographie",
        "index-np",
    ]
    assert coquille.chapters[0].url == "http://urlDuLivreintroduction.html"


def test_chapters_authors(coquille_chapter):
    assert len(coquille_chapter.authors) == 2
    assert [
        f"{author.forname} {author.surname}" for author in coquille_chapter.authors
    ] == ["Marcello Vitali Rosati", "Antoine Fauchié"]
