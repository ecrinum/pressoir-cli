# Changelog

## 3.3.1 — 2025-02-11

* Do not try to copy `img` if not present in collection

## 3.3.0 — 2025-02-01

* First step toward a book generation from Stylo

## 3.2.0 — 2025-01-21

* Better implementation of PDF exports with bibliography and references

## 3.1.0 — 2025-01-17

* Basic implementation of PDF exports

## 3.0.2 — 2025-01-11

* Fixes link to the first chapter from the homepage
* Hides ISBN from the homepage if not present in `livre.yaml`
* Fixes some HTML rendering within the documentation

## 3.0.1 — 2024-12-10

* Fixes the rendering of `code > pre` within additional contents

## 3.0.0 — 2024-12-10

* Rename the package from pressoir-cli to pressoir


## 2.2.5 — 2024-11-19

* Deal with dynamic indexes defined in `book.toml`
* Link to first chapter of the book from homepage
* Fix homepage title html tag content

## 2.2.4 — 2024-11-10

* Default blank collection for init
* Homepage is not generated if source does not exists

## 2.2.3 — 2024-09-16

* Rename couv image for docs

## 2.2.2 — 2024-09-16

* Cast a Python Path explicitely

## 2.2.1 — 2024-09-16

* Explicit target for docs generation

## 2.2.0 — 2024-09-16

* Generate a homepage when building a book

## 2.1.0 — 2024-09-16

* Generate documentation using Pressoir itself
* Do not cite directors if no collective book
* Transform citations, even out of contenus add

## 2.0.1 — 2024-03-14

* Use relative URLs for book’s navigation

## 2.0.0 — 2024-03-13

* Evolution of source YAML files format to avoid redudancies in keys
* Better empty book shell for demo purpose
* Huge refactor of internals to speed up book’s generation

## 1.6.0 — 2024-02-21

* Ability to modify chevron-related SVG colors

## 1.5.0 — 2024-02-21

* Ability to set glossaire settings for label/title

## 1.4.0 — 2024-02-07

* Better targeting of color replacemens in CSS+SVG
* Dynamically use indexes from book.toml in JS

## 1.3.0 — 2024-02-06

* Allow to display pressoir-cli’s current version

## 1.2.0 — 2024-02-06

* Automatize colors replacement for theme
* Hide the block for translation if there is none

## 1.1.0 — 2024-01-31

* Make the `repository` parameter optional

## 1.0.0 — 2024-01-18

* Extract code from the previous Pressoir to make a standalone script
