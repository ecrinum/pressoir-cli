<!--insérer ci-dessous le texte en markdown du chapitre 1-->

## I. Titre de section


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse risus velit, dignissim et ante a, volutpat elementum ipsum[^1].

### I.1. Titre de sous-section

[Etiam non](#monancre) tortor ac turpis sagittis lobortis. Phasellus nec diam vitae sem efficitur euismod.

!contenuadd(./idCA4)

<!-- pour insérer un contenu additionnel dans le corps du texte-->

### I.2. Titre de sous-section

In feugiat tincidunt sagittis. Fusce venenatis egestas neque quis tempor[^2].


## II. Titre de section

Pellentesque pulvinar eros non est pretium[^3], pharetra auctor orci. Etiam accumsan orci vitae convallis ultrices.





[^1]: Voir @bonnetQuEstceQue2023.

[^2]: Voir [chapitre 2](chapitre2.html).

[^3]: Cf. cette vidéo&nbsp;:    
   <iframe src="https://www.youtube.com/embed/GI2GuRaD1hU?si=JaekRNgO2dwRYRl5" title="Stylo: un éditeur sémantique pour les humanités" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>


## Contenus additionnels

<!-- si pas de CA, supprimer le titre de niveaux 2 -->

!contenuadd(./idCA5)

!contenuadd(./idCA6)


## Références

<!-- C'est ici que s'afficheront les références bibliographiques du fichier introduction.bib dans le html-->
