## idCA4

---
title: >-
   Titre du contenu additionnel 4 : exemple illustration
credits: >-
   Les Ateliers de [sens public]
keywords: livre,photo,exemple
lang: fr
type: image
link: media/imagelivre.jpeg
link-archive:
embed:
zotero:
date: 2024-01-12
date-publication: 2024-03-06
source: auteur
priority: lowpriority
position: main

---

J'ai ici mis une image d'un livre ouvert, en contenu additionnel fermé par défaut.



## idCA5

---
title: >-
   Titre du contenu additionnel 5 : exemple de tableau
credits: >-
   Institut de Statistiques VVV
keywords: tableau,statistiques,chiffres
lang: fr
type: article
link:
link-archive:
embed:
zotero:
date: 2020-09-02
date-publication: 2024-03-06
source: auteur
priority: lowpriority
position: main

---

Id neque aliquam vestibulum morbi blandit cursus risus at ultrices. [Ultrices mi tempus]{#monancre} imperdiet nulla malesuada pellentesque elit.

|Année|Lieu|Couleur|Chiffre 1|Pourcentage|
|:--|:--|:-:|-:|--:|
|2007|Montréal|vert|3|6,8%|
|1923|Toronto|jaune|49|27,3%|
|1976|Vancouver|bleu|107|86%|

Etiam erat velit scelerisque in dictum non consectetur a erat.


## idCA6

---
title: >-
   Titre du contenu additionnel 6 : exemple de pageWeb
credits: >-
   Wikipédia
keywords: Marcello Vitali Rosati,Université de Montréal,Écritures numériques
lang: fr
type: pageWeb
link: https://fr.wikipedia.org/wiki/Marcello_Vitali-Rosati
link-archive: https://fr.wikipedia.org/w/index.php?title=Marcello_Vitali-Rosati&oldid=206586659
embed:
zotero:
date: 2024-03-06
date-publication: 2024-03-06
source: éditeur
priority: lowpriority
position: main

---

Ce site internet est très intéressant pour approfondir le sujet dont il est question dans ce chapitre.
