## idCA1

---
title: >-
   Séminaire IMPEC 2019 - Hélène BEAUCHEF et Servanne MONJOUR - Les Ateliers de [sens public]
credits: >-
   Laboratoire ICAR
keywords: séminaire,IMPEC,Hélène Beauchef,Servanne Monjour,
lang: fr
type: video
link: https://youtu.be/TvlxJshxLVc?si=L3w_GIWdJEcQYL6Q
link-archive:
embed: https://www.youtube.com/embed/TvlxJshxLVc?si=w5aB0-Pa9h6_rs65
zotero:
date: 2023-04-04
date-publication: 2024-03-06
source: auteur
priority: lowpriority
position: main

---

Ici il est possible -- mais pas obligatoire -- d'ajouter un texte.


## idCA2

---
title: >-
   Titre du contenu additionnel 2 (ex : texte)
credits: >-
   ici les crédits
keywords: ici,les,mots,cles
lang: fr
type: article
link:
link-archive:
embed:
zotero:
date: 2024-02-01
date-publication: 2024-03-06
source: éditeur
priority: highpriority
position: main

---

Enim tortor at auctor urna nunc id cursus. Maecenas pharetra convallis posuere morbi leo urna molestie. Eget egestas purus viverra accumsan in nisl nisi. Gravida quis blandit turpis cursus. Nam at lectus urna duis convallis convallis tellus.


## idCA3

---
title: >-
   Titre du contenu additionnel 3 : exemple lien externe
credits: >-
   Les Ateliers de [sens public]
keywords: édition,Montréal,lowtech
lang: fr
type: pageWeb
link: https://ateliers.sens-public.org/index.html
link-archive:
embed:
zotero:
date: 2024-02-01
date-publication: 2024-03-06
source: éditeur
priority: lowpriority
positon: main

---

Accéder au site de la maison d'édition des [Ateliers de \[sens public\]](https://ateliers.sens-public.org/index.html){link-archive="https://web.archive.org/web/20240306143153/https://ateliers.sens-public.org/index.html"}.


## idCA4

---
title: >-
   Titre du contenu additionnel 4 : exemple illustration
credits: >-
   Les Ateliers de [sens public]
keywords: livre,photo,exemple
lang: fr
type: image
link: media/imagelivre.jpeg
link-archive:
embed:
zotero:
date: 2024-01-12
date-publication: 2024-03-06
source: auteur
priority: lowpriority
positon: main

---

J'ai ici mis une image d'un livre ouvert, en contenu additionnel fermé par défaut.
