<!--insérer ci-dessous le texte en markdown du chapitre 3-->

## I. Titre de section

Ceci est un exemple de chapitre sans section **Contenus additionnels** et  **Références**.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse risus velit, dignissim et ante a, volutpat elementum ipsum[^1].

### I.1. Titre de sous-section

Etiam non tortor ac turpis sagittis lobortis. Phasellus nec diam vitae sem efficitur euismod.


### I.2. Titre de sous-section

In feugiat tincidunt sagittis. Fusce venenatis egestas neque quis tempor.


## II. Titre de section

Pellentesque pulvinar eros, pharetra auctor orci. Etiam accumsan orci vitae convallis ultrices.


[^1]: Integer ut ante et lacus.
