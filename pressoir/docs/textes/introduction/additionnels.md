## balisageInfraTextuel

---
title: >-
   Balisage infra-textuel
credits:
keywords: balisage,index,fonctionnalités
lang: fr
type: article
link:
link-archive:
embed:
zotero:
date: 2023-09-30
date-publication: 2024-09-30
source: auteur
priority: lowpriority
position: main

---

Le pictogramme [+] présent dans le paragraphe ci-dessus indique l'utilisation d'une des fonctionnalités développées pour le Pressoir : le balisage infra-textuel qui permet d'identifier des termes sélectionnés (ici, des personnes) et de les relier à des autorités (ici, l'identifiant unique [ORCID](https://orcid.org/)).

Pour en savoir plus sur cette fonctionnalité, voir la section [Balisage infra-textuel](chapitre2.html#balisage-infra-textuel).
