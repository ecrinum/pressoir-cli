Le Pressoir est un générateur de sites statiques qui peut être utilisé pour créer des livres augmentés, des supports de cours, des documentations (comme celle-ci) et toutes sortes de publications numériques.

Il a été initié par la [Chaire de recherche du Canada sur les écritures numériques](https://www.ecrituresnumeriques.ca/fr).

Le code est [disponible en *open source* sous licence GPLv3](https://gitlab.huma-num.fr/ecrinum/pressoir/).


Cette documentation comprend toutes les informations sur ce projet ainsi qu'un guide pour utiliser le Pressoir et créer un ouvrage.


<p class="cta"><a href="introduction.html">Lire la documentation</a></p>
