## EnvironnementVirtuel

---
title: >-
   Installer un environnement virtuel
credits:
keywords: venv,environnement,virtuel,python
lang: fr
type: article
link:
link-archive:
embed:
zotero:
date: 2024-09-20
date-publication: 2024-09-20
source: auteur
priority: lowpriority
position: main
---

Au besoin, installer et activer un environnement virtuel&nbsp;:

```
$ python3 -m venv venv
$ source venv/bin/activate
```


## CreditsDoc

---
title: >-
   Crédits pour la documentation
credits:
keywords: crédits, documentation
lang: fr
type: article
link:
link-archive:
embed:
zotero:
date: 2024-09-20
date-publication: 2024-09-20
source: auteur
priority: highpriority
position: main
---

Les polices *Averia Libre* et *Averia Serif Libre* sont sous licence SIL Open Font License.

L’image d’entête provient de [Wikipedia](https://en.wikipedia.org/wiki/History_of_printing#/media/File:Printer_in_1568-ce.png).
